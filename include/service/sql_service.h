#ifndef SERVICE_SQL_CONNECTION_H
#define SERVICE_SQL_CONNECTION_H

#include "external_service.h"

void set_timeout(const int t);

int get_timeout();

int execute_statement(const char *sql);

#endif