#ifndef SERVICE_CONNECTION_DEFAULT_H
#define SERVICE_CONNECTION_DEFAULT_H

#include "external_service.h"

int send(char *msg);

int receive(int bytes, char *output);

#endif