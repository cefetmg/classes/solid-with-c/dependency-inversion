#include "service/socket_service.h"
#include <stdio.h>
#include <stdlib.h>

int ping(const char* message) {
    char enh_message[100];
    sprintf(enh_message, "[default_connection_service] %s\n", message);
    fprintf(stderr, "%s", enh_message);
    return 1;
}

int receive(int bytes, char *output) {
    sprintf(output, "Writing %d bytes to output", bytes);
    return 0;
}