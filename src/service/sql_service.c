#include "service/sql_service.h"
#include <stdio.h>
#include <stdlib.h>

int timeout = 0;

int ping(const char* message) {
    char enh_message[100];
    sprintf(enh_message, "[sql_connection_service] %s\n", message);
    fprintf(stderr, "%s", enh_message);
    return 0;
}

void set_timeout(const int t) {
    timeout = (int) t;
}

const int get_timeout() {
    return timeout;
}

int execute_statement(const char *sql) {
    printf("Running query %s\n. Timeout: %d\n", sql, get_timeout());
    return 0;
}