#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int should_call_ping_on_each_specific_library() {
    // Add the class names to a list
    const char* lib_names[] = {"./libsocket_service.so", "./libsql_service.so", NULL};
    const int expected_ret_value[] = {1, 0};
    const char* lib_name;
    int i;

    for (i = 0; lib_names[i] != NULL; i++) {
        lib_name = lib_names[i];

        void *object;
        int (*ping)(const char*);

        // Instantiate the object by name
        object = dlopen(lib_name, RTLD_LAZY);
        assert(object);

        // Load the -> method <-
        ping = dlsym(object, "ping");
        assert(ping);

        // Method call        
        assert(ping("ping the expected library") == expected_ret_value[i]);

        // Delete the object
        assert(dlclose(object) == 0);
    }
    return 0;
}

int main() {
    return should_call_ping_on_each_specific_library();
}