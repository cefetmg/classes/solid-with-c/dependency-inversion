#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <assert.h>

int should_load_message_from_object_dynamically_loaded() {
    char buf[100];
    void (*receive)(int, char *);

    // Instiantiate another object
    void *socket = dlopen("./libsocket_service.so", RTLD_LAZY);
    assert(socket);

    // Get method
    receive = dlsym(socket, "receive");
    assert(receive);

    receive(100, buf);
    assert(!strcmp("Writing 100 bytes to output", buf));

    assert(dlclose(socket) == 0);
    return 0;
}

int main() {
    return should_load_message_from_object_dynamically_loaded();
}