#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int expect_timeout(void* object, const int expected_timeout) {
    const int (*get_timeout)(void);
    get_timeout = dlsym(object, "get_timeout");
    assert(get_timeout);

    const int actual_timeout = get_timeout();

    assert(actual_timeout == expected_timeout);
    return 0;
}

int set_timeout(void* object, const int expected_timeout) {
    void (*set_timeout)(int);
    set_timeout = dlsym(object, "set_timeout");
    assert(set_timeout);

    set_timeout(expected_timeout);
    return 0;
}

int should_load_two_objects_in_different_namespaces() {
    // Instiantiate an object
    void *short_timeout_obj = dlmopen(LM_ID_NEWLM, "./libsql_service.so", RTLD_LAZY);
    assert(short_timeout_obj);

    // Instiantiate another object
    void *long_timeout_obj = dlmopen(LM_ID_NEWLM, "./libsql_service.so", RTLD_LAZY);
    assert(long_timeout_obj);

    expect_timeout(short_timeout_obj, 0);
    expect_timeout(long_timeout_obj, 0);

    set_timeout(short_timeout_obj, 10);
    set_timeout(long_timeout_obj, 60);

    expect_timeout(short_timeout_obj, 10);
    expect_timeout(long_timeout_obj, 60);

    set_timeout(short_timeout_obj, 35);

    expect_timeout(short_timeout_obj, 35);
    expect_timeout(long_timeout_obj, 60);

    assert(dlclose(short_timeout_obj) == 0);
    assert(dlclose(long_timeout_obj) == 0);
    return 0;
}

int should_load_two_objects_in_same_namespace() {
    // Instiantiate an object
    void *short_timeout_obj = dlopen("./libsql_service.so", RTLD_LAZY);
    assert(short_timeout_obj);

    // Instiantiate another object
    void *long_timeout_obj = dlopen("./libsql_service.so", RTLD_LAZY);
    assert(long_timeout_obj);

    expect_timeout(short_timeout_obj, 0);
    expect_timeout(long_timeout_obj, 0);

    set_timeout(short_timeout_obj, 10);
    set_timeout(long_timeout_obj, 60);

    expect_timeout(short_timeout_obj, 60);
    expect_timeout(long_timeout_obj, 60);

    set_timeout(short_timeout_obj, 35);

    expect_timeout(short_timeout_obj, 35);
    expect_timeout(long_timeout_obj, 35);

    assert(dlclose(short_timeout_obj) == 0);
    assert(dlclose(long_timeout_obj) == 0);
    return 0;
}

int main() {
    return should_load_two_objects_in_different_namespaces() | should_load_two_objects_in_same_namespace();
}